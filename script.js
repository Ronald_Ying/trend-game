const labels = [
];
const data = {
    labels: labels,
    datasets: [{
        label: "APPLE",
        backgroundColor: "rgb(255, 99, 132)",
        borderColor: "rgb(255, 99, 132)",
        data: [],
    }]
};
const config = {
    type: "line",
    data: data,
    options: {}
};
const myChart = new Chart(
    document.getElementById("myChart"),
    config
);
function money(){
    let userMoney = document.getElementById("userMoney");
    userMoney.textContent = 100
}
function addDataEveryFourSecond() {
    let timeLeft = 3;
    setInterval(function () {
        if (timeLeft < 0) {
            timeLeft = 3;
        }
        if (timeLeft === 0) {
            addData(myChart);
        }
        document.querySelector(".timer").textContent = timeLeft;
        timeLeft -= 1;
    }, 1000);
}
function addData(chart) {
    chart.data.labels.push(new Date().toLocaleTimeString());
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(Math.floor(Math.random() * 101));
        let priceData = dataset.data;
        let priceLength = priceData.length;
        let lastUpdatePrice = priceData.length;
        updatedPrice(dataset.data[priceLength - 1]);
        compareToLastUpdate(priceData[priceLength - 1], priceData[lastUpdatePrice - 2])
    });
    chart.update();
}
function updatedPrice(price){
    let stockPrice = document.getElementById("stockPrice");
    stockPrice.textContent = price;
}
function compareToLastUpdate(currentPrice, lastUpdatePrice){
    let comparePrice = document.getElementById("comparePrice");
    let profit = parseInt(currentPrice) - parseInt(lastUpdatePrice);
    if(profit >0){
        comparePrice.textContent = "+" + profit;
        comparePrice.classList.remove("losing-money");

    }else{
        comparePrice.textContent = profit;
        comparePrice.classList.add("losing-money");
    }
    calcProfit(profit);
}
function add() {
    let betMoney = document.getElementById("betMoney")
    let userMoney = document.getElementById("userMoney");
    if (parseInt(userMoney.textContent) > parseInt(betMoney.textContent)) {
        betMoney.textContent = parseInt(betMoney.textContent) + 2;
    }
}
function minus() {
    let betMoney = document.getElementById("betMoney");
    let userMoney = document.getElementById("userMoney");
    let amountOfMoney = (parseInt(betMoney.textContent) - parseInt(betMoney.textContent) - parseInt(betMoney.textContent))
    if (parseInt(userMoney.textContent) > amountOfMoney) {
        betMoney.textContent = parseInt(betMoney.textContent) - 2;
    }
}
function allinRaise(){
    let betMoney = document.getElementById("betMoney");
    let userMoney = document.getElementById("userMoney");
    if (parseInt(userMoney.textContent) > parseInt(betMoney.textContent)) {
        betMoney.textContent = parseInt(userMoney.textContent);
    }
}
function allinFall(){
    let betMoney = document.getElementById("betMoney");
    let userMoney = document.getElementById("userMoney");
    let amountOfMoney = (parseInt(betMoney.textContent) - parseInt(betMoney.textContent) - parseInt(betMoney.textContent))
    if (parseInt(userMoney.textContent) > amountOfMoney) {
        betMoney.textContent = parseInt(userMoney.textContent) - parseInt(userMoney.textContent) - parseInt(userMoney.textContent);
    }
}
function win(){
    let betMoney = document.getElementById("betMoney");
    let userMoney = document.getElementById("userMoney");
    if(parseInt(betMoney.textContent) > 0){
        userMoney.textContent = parseInt(userMoney.textContent) + parseInt(betMoney.textContent);
    }else{
        userMoney.textContent = parseInt(userMoney.textContent) - parseInt(betMoney.textContent);
    }
}
function lose(){
    let betMoney = document.getElementById("betMoney");
    let userMoney = document.getElementById("userMoney");
    if(parseInt(betMoney.textContent) > 0){
        userMoney.textContent = parseInt(userMoney.textContent) - parseInt(betMoney.textContent);
    }else{
        userMoney.textContent = parseInt(userMoney.textContent) + parseInt(betMoney.textContent);
    }
}

function calcProfit(profit){
    let betMoney = document.getElementById("betMoney");
    if(profit > 0 && parseInt(betMoney.textContent) > 0){
        win();
        console.log(`You won ${betMoney.textContent}$`);
        betMoney.textContent = 0;
    }else if(profit < 0 && parseInt(betMoney.textContent) > 0){
        lose();
        console.log(`You lose ${betMoney.textContent}$`);
        betMoney.textContent = 0;
    }else if(profit > 0 && parseInt(betMoney.textContent) < 0){
        lose();
        console.log(`You lose ${parseInt(betMoney.textContent) - parseInt(betMoney.textContent) - parseInt(betMoney.textContent)}$`);
        betMoney.textContent = 0;
    }else if(profit < 0 && parseInt(betMoney.textContent) < 0){
        win();
        console.log(`You won ${parseInt(betMoney.textContent) - parseInt(betMoney.textContent) - parseInt(betMoney.textContent)}$`);
        betMoney.textContent = 0;
    }else if(profit === 0){
        lose();
        console.log(`You lose ${betMoney.textContent}$`);
        betMoney.textContent = 0;
    }else{
        console.log("You did not bet any money");
    }
}
addDataEveryFourSecond();
addData(myChart);
money();











